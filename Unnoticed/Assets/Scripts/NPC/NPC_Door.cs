﻿using UnityEngine;
using System.Collections;

public class NPC_Door : MonoBehaviour {

    private CapsuleCollider capsule;

    void Awake()
    {
        capsule = GetComponent<CapsuleCollider>();
    }

    // Lässt eine Tür aufmachen, falls ein Gegner darauf trifft, da NavMesh Agents Collider ignorieren
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(capsule.transform.position, capsule.transform.forward, out hit, 1f))
        {
            GameObject thing = hit.collider.gameObject;
            if (thing.tag == Tags.door)
            {
                if(!thing.GetComponentInParent<Door>().open)
                    hit.collider.gameObject.GetComponentInParent<Door>().TurnDoor(capsule.transform.position);
            }
        }
    }
}
