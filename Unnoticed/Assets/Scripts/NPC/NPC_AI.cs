﻿using UnityEngine;
using System.Collections;
using System;

public class NPC_AI : MonoBehaviour
{
    public Transform[] patrolWayPoints;
    [HideInInspector]
    public float alertScale = 1f;
    
    [HideInInspector]
    public float alert;
    [HideInInspector]
    public float previousAlert;
    [HideInInspector]
    public float waitTimer;
    public float alertedWaitTime = 10f;

    [HideInInspector]
    public float breakpointLooking = 1.5f;
    private float breakpointFollow = 8f;
    private float stepAlertFactor = 2f;
    private float jumpAlertFactor = 3f;

    private NPC_Sight npcSight;
    private NavMeshAgent nav;
    private SphereCollider col;
    private Transform player;
    private Player_Health playerHealth;
    private Player_Movement playerMovement;
    private AudioAssets audioAssets;

    public enum AI_State
    {
        inactive,
        idle,
        patrolling,
        looking,
        following,
        shooting
    }

    [Header("AI States")]
    public AI_State npcState = AI_State.idle;
    public bool allowToShoot = true;
    public bool allowToFollow = true;
    public bool allowToLook = true;
    public bool allowToPatrol = true;
    
    void Awake()
    {
        // Zuweisung von Variablen
        #region
        npcSight = GetComponent<NPC_Sight>();
        nav = GetComponent<NavMeshAgent>();
        col = GetComponent<SphereCollider>();
        player = GameObject.FindGameObjectWithTag(Tags.player).transform;
        playerHealth = player.GetComponent<Player_Health>();
        playerMovement = player.GetComponent<Player_Movement>();
        audioAssets = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<AudioAssets>();
        alert = 0f;
        waitTimer = 0f;
        #endregion

        // Zuweisung der Delegaten
        #region
        PlayerTracker.Stepped += ReactToStep;
        PlayerTracker.Jumped += ReactToJump;
        PlayerTracker.ShotFired += ReactToShot;
        PlayerTracker.DoorMoved += ReactToDoorSound;
        #endregion
    }


    void Update()
    {
        if (playerHealth.health <= 0.0f)
            alert = 0.0f;

        PlayVoiceSounds();
        UpdateAlert();
        UpdateState();
    }
    
    // Passt den Alarmstatus der KI mithilfe des Alarmfaktors an
    // Falls der Spieler nicht in Sicht ist, so wird der Alarmstatus erst nach 
    // Ablauf eines Timers auf 0 gesetzt
    void UpdateAlert()
    {
        previousAlert = alert;
        if (npcSight.playerInSight)
        {
            alert += Time.deltaTime * alertScale;
            waitTimer = alertedWaitTime;
            if (alert > breakpointFollow)
                waitTimer = alertedWaitTime * 1.5f;
        }
        else
        {
            if(waitTimer > 0)
            {
                waitTimer -= Time.deltaTime;
            }else if(waitTimer < 0)
            {
                alert = 0;
            }
        }
        alert = Mathf.Clamp(alert, 0f, 50f);
    }

    // Passt die Verhaltensweise der KI je nach Alarmstatus an
    void UpdateState()
    {
        if(!allowToPatrol && !allowToLook && !allowToFollow && !allowToShoot)
        {
            npcState = AI_State.idle;
        }
        else
        {
            if (alert >= breakpointFollow)
            {
                if (npcSight.playerInSight)
                {
                    if (allowToShoot)
                        npcState = AI_State.shooting;
                }
                else
                {
                    if (allowToFollow)
                        npcState = AI_State.following;
                }
            }
            else if (alert >= breakpointLooking)
            {
                if (allowToLook)
                    npcState = AI_State.looking;
            }
            else
            {
                if (patrolWayPoints.Length > 0 && NoNullWaypoints() == true) {
                    if (allowToPatrol)
                        npcState = AI_State.patrolling;
                }
                else
                    npcState = AI_State.idle;
            }
        }
    }

    // Überprüft ob die Liste der Patroullierpunkte keine "leeren" Einträge enthält
    bool NoNullWaypoints()
    {
        for(int i = 0; i < patrolWayPoints.Length; i++)
        {
            if (patrolWayPoints[i] == null)
                return false;
        }
        return true;
    }

    // Spielt einen Clip ab, falls die KI alamiert wurde oder wenn der Alarmstatus zurückgesetzt wurde
    void PlayVoiceSounds()
    {
        if(alert > breakpointLooking && previousAlert < breakpointLooking)
        {
            AudioClip[] clips = audioAssets.voicesIntrigued;
            AudioSource.PlayClipAtPoint(clips[UnityEngine.Random.Range(0, clips.Length)], transform.position, 5f);
        }else if(alert < breakpointLooking && previousAlert > breakpointLooking)
        {
            AudioClip[] clips = audioAssets.voicesCalmed;
            AudioSource.PlayClipAtPoint(clips[UnityEngine.Random.Range(0, clips.Length)], transform.position, 5f);
        }
    }
    
    // Erhöht den Alarmstatus falls ein Sprung in der Nähe gehört wurde
    public void ReactToStep(Vector3 position, float intensity)
    {
        if (!playerMovement.Crouching)
        {
            float distance = Toolbox.Spatial.CalcPathDistance(transform.position, position, ref nav);
            if (distance <= col.radius)
            {
                alert += stepAlertFactor * (1 - distance / col.radius);
                waitTimer = alertedWaitTime * 0.5f;
                if (alert > breakpointLooking)
                {
                    waitTimer = alertedWaitTime;
                    if (alert > breakpointFollow)
                        waitTimer = alertedWaitTime * 1.5f;
                }
            }
        }
    }

    // Erhöht den Alarmstatus falls Schritte in der Nähe gehört wurden
    public void ReactToJump(Vector3 position, float intensity)
    {
        if (!playerMovement.Crouching)
        {
            float distance = Toolbox.Spatial.CalcPathDistance(transform.position, position, ref nav);
            if (distance <= col.radius && intensity > 1f)
            {
                alert += intensity * (1 - distance / col.radius) * jumpAlertFactor;
                waitTimer = alertedWaitTime * 0.5f;
                if (alert > breakpointLooking)
                {
                    npcSight.targetSightingPosition = position;
                    waitTimer = alertedWaitTime;
                    if (alert > breakpointFollow)
                        waitTimer = alertedWaitTime * 1.5f;
                }
            }
        } 
    }

    // Erhöht den Alarmstatus falls Schüsse in der Nähe gehört wurden
    public void ReactToShot(Vector3 position, float intensity)
    {
        float distance = Toolbox.Spatial.CalcPathDistance(transform.position, position, ref nav);
        if(distance <= col.radius * 1.5 && !npcSight.playerInSight)
        {
            alert += breakpointFollow;
            npcSight.targetSightingPosition = position;
            waitTimer = 15f;
        }
    }

    // Erhöht den Alarmstatus leicht falls gehört wurde, wie eine Tür auf/zugeht
    public void ReactToDoorSound(Vector3 position, float intensity)
    {
        float distance = Toolbox.Spatial.CalcPathDistance(transform.position, position, ref nav);
        if(distance <= col.radius)
        {
            float ratio = 1 - (distance / col.radius);
            alert += breakpointLooking * ratio;
            waitTimer = alertedWaitTime * 0.5f;
            if (alert > breakpointLooking)
            {
                npcSight.targetSightingPosition = position;
                waitTimer = alertedWaitTime;
                if (alert > breakpointFollow)
                    waitTimer = alertedWaitTime * 1.5f;
            }
        }
    }
}