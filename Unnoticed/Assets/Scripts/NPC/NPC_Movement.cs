﻿using UnityEngine;
using System.Collections;

public class NPC_Movement : MonoBehaviour
{
    public float deadZone = 5f;
    public float patrolSpeed = 1.5f;
    public float followSpeed = 5.6f;
    public float followWaitTime = 5f;
    public float patrolWaitTime = 3.0f;

    [Header("footing")]
    public Transform rightFoot;
    public Transform leftFoot;
    public AudioClip stepSound;
    [Range(0f, 0.5f)]
    public float stepTriggerHeight = 0.15f;

    private bool playStepSoundLeft = false;
    private bool playStepSoundRight = false;

    [Space]

    [Range(0f, 5f)]
    public float moveSpeedLerpSmoothness = 1.2f;

    private Vector3 initialPosition;
    private Vector3 initialForward;
    
    private PlayerTracker playerTracker;          
    private NPC_Sight npcSight;
    private NPC_AI npcAI;       
    private NavMeshAgent nav;               
    private Animator anim;                  
    private HashIDs hash;

    private float followingTimer;
    private float patrolTimer;
    private int wayPointIndex;
    
    [Range(0f, 1f)]
    public float speedDampTime = 0.1f;              // Damping time for the Speed parameter.
    [Range(0f, 1f)]
    public float angularSpeedDampTime = 0.7f;       // Damping time for the AngularSpeed parameter
    [Range(0f, 1f)]
    public float angleResponseTime = 0.6f;          // Response time for turning an angle into angularSpeed.
    
    void Awake()
    {
        // Zuweisung von Variablen
        #region
        initialPosition = new Vector3(transform.position.x, 0f, transform.position.z);
        initialForward = transform.forward;
        playerTracker = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<PlayerTracker>();
        npcSight = GetComponent<NPC_Sight>();
        npcAI = GetComponent<NPC_AI>();
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
        #endregion

        nav.updateRotation = false;
        
        anim.SetLayerWeight(1, 0.9f);
        anim.SetLayerWeight(2, 1f);
        
        deadZone *= Mathf.Deg2Rad;
    }
    
    // Stellt das Bewegungsverhalten je nach KI Zustand um
    void Update()
    {
        StepSounds();

        float angle = 0f;
        float speed = 0f;

        switch (npcAI.npcState)
        {
            #region Lässt den Charakter nur auf der Stelle stehen
            case NPC_AI.AI_State.inactive:
                nav.Stop();
                speed = 0f;
                angle = 0f;
                break;
            #endregion

            #region Lässt den Charakter auf der Stelle stehen. Kehrt zum Startpunkt zurück, falls dieser sich zu weit entfernt hat
            case NPC_AI.AI_State.idle:
                anim.SetBool(hash.npcShootingBool, false);
                Vector3 deltaPos = initialPosition - transform.position;
                speed = Vector3.Project(nav.desiredVelocity, transform.forward).magnitude;
                angle = FindAngle(transform.forward, nav.desiredVelocity, transform.up);
                if (deltaPos.sqrMagnitude > 4f)
                    nav.destination = initialPosition;

                nav.speed = patrolSpeed;

                if (nav.remainingDistance < nav.stoppingDistance)
                {
                    angle = FindAngle(transform.forward, initialForward, Vector3.up);
                }

                break;
            #endregion

            #region Lässt den Gegner entlang einer Route laufen und bei jedem Punkt für eine gewisse Zeit warten
            case NPC_AI.AI_State.patrolling:
                anim.SetBool(hash.npcShootingBool, false);
                speed = Vector3.Project(nav.desiredVelocity, transform.forward).magnitude;
                angle = FindAngle(transform.forward, nav.desiredVelocity, transform.up);

                nav.speed = patrolSpeed;

                if (nav.destination == playerTracker.resetPosition || nav.remainingDistance < nav.stoppingDistance)
                {
                    patrolTimer += Time.deltaTime;

                    if (patrolTimer >= patrolWaitTime)
                    {
                        if (wayPointIndex == npcAI.patrolWayPoints.Length - 1)
                            wayPointIndex = 0;
                        else
                            wayPointIndex++;

                        patrolTimer = 0;
                    }
                }
                else
                {
                    patrolTimer = 0;
                }

                if (npcAI.patrolWayPoints.Length > 0 && npcAI.patrolWayPoints[wayPointIndex] != null)
                    nav.destination = npcAI.patrolWayPoints[wayPointIndex].position;
                break;
            #endregion

            #region Lässt den Gegner in eine bestimmte Richtung blicken
            case NPC_AI.AI_State.looking:
                anim.SetBool(hash.npcShootingBool, false);
                speed = 0f;
                angle = FindAngle(transform.forward, npcSight.targetSightingPosition - transform.position, transform.up);
                break;
            #endregion

            #region Lässt den Gegner an einen bestimmten Punkt laufen
            case NPC_AI.AI_State.following:
                anim.SetBool(hash.npcShootingBool, false);
                Vector3 sightingDeltaPos1 = npcSight.targetSightingPosition - transform.position;
                speed = Vector3.Project(nav.desiredVelocity, transform.forward).magnitude;
                angle = FindAngle(transform.forward, nav.desiredVelocity, transform.up);
                if (sightingDeltaPos1.sqrMagnitude > 4f)
                    nav.destination = npcSight.targetSightingPosition;

                nav.speed = followSpeed;

                if (nav.remainingDistance < nav.stoppingDistance)
                {
                    followingTimer += Time.deltaTime;

                    if (followingTimer % (followWaitTime * 0.3f) == 0f)
                    {
                        angle = Random.Range(0f, 360f);
                    }

                    if (followingTimer >= followWaitTime)
                    {
                        playerTracker.globalLastKnownPosition = playerTracker.resetPosition;
                        npcSight.targetSightingPosition = playerTracker.resetPosition;
                        followingTimer = 0f;
                    }
                }
                else
                    followingTimer = 0f;
                break;
            #endregion

            #region Lässt den Gegner stehen bleiben und schießen
            case NPC_AI.AI_State.shooting:
                angle = FindAngle(transform.forward, npcSight.targetSightingPosition - transform.position, transform.up);
                if (npcSight.playerInSight)
                {
                    anim.SetBool(hash.npcShootingBool, true);
                }
                else
                {
                    anim.SetBool(hash.npcShootingBool, false);
                }
                break;
                #endregion
        }

        if (Mathf.Abs(angle) < deadZone)
        {
            //transform.LookAt(transform.position + nav.desiredVelocity);
            angle = 0f;
        }
        
        // Angular speed is the number of degrees per second.
        float angularSpeed = angle / angleResponseTime;
        
        anim.SetFloat(hash.speedFloat, speed, speedDampTime, Time.deltaTime);
        anim.SetFloat(hash.angularSpeedFloat, angularSpeed, angularSpeedDampTime, Time.deltaTime);
    }

    // Anpassung der Geschwindigkeit und Rotation des GameObject an die Animation
    void OnAnimatorMove()
    {
        if(Mathf.Abs(Time.deltaTime) > 0)
        {
            nav.velocity = anim.deltaPosition / Time.deltaTime;
            transform.rotation = anim.rootRotation;
        }
    }

    // Spielt Schrittgeräusche ab wenn sich der Gegner bewegt
    void StepSounds()
    {
        if (leftFoot != null && rightFoot != null && nav.velocity.magnitude > 1f)
        {
            if (leftFoot.transform.position.y - transform.position.y < stepTriggerHeight)
            {
                if (playStepSoundLeft)
                {
                    AudioSource.PlayClipAtPoint(stepSound, leftFoot.position, (nav.velocity.magnitude - 1) / 4);
                    playStepSoundLeft = false;
                }
            }
            else
                playStepSoundLeft = true;

            if(rightFoot.transform.position.y - transform.position.y < stepTriggerHeight)
            {
                if (playStepSoundRight)
                {
                    AudioSource.PlayClipAtPoint(stepSound, rightFoot.position, (nav.velocity.magnitude - 1) / 4);
                    playStepSoundRight = false;
                }
            }
            else
                playStepSoundRight = true;
        }
    }

    // Berechnet den Winkel zwischen 2 Vektoren
    float FindAngle(Vector3 fromVector, Vector3 toVector, Vector3 upVector)
    {
        if (toVector == Vector3.zero)
            return 0f;

        float angle = Vector3.Angle(fromVector, toVector);
        Vector3 normal = Vector3.Cross(fromVector, toVector);
        angle *= Mathf.Sign(Vector3.Dot(normal, upVector));
        angle *= Mathf.Deg2Rad;

        return angle;
    }
}
