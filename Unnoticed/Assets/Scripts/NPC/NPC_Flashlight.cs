﻿using UnityEngine;
using System.Collections;

public class NPC_Flashlight : MonoBehaviour {
    
    public float lightAngle = 45.0f;
    public float lightMaxIntensity = 1.0f;
    public AudioClip soundFlashLightOn;
    public AudioClip soundFlashLightOff;
    [HideInInspector]
    public bool playerIlluminated = false;

    private NPC_AI npcAI;
    private NPC_Sight npcSight;
    private Light flash;
    private GameObject playerHead;
    
	void Awake()
    {
        npcAI = GetComponentInParent<NPC_AI>();
        npcSight = GetComponentInParent<NPC_Sight>();
        flash = GetComponentInChildren<Light>();
        if(flash.type == LightType.Spot)
            flash.spotAngle = lightAngle;
        playerHead = GameObject.FindGameObjectWithTag(Tags.playerHead);
    }

    void Update()
    {
        playerIlluminated = CheckPlayerIllumination();

        // Dreht das Licht auf den Spieler, um seltsame Haltung zu vermeiden
        if (npcAI.npcState == NPC_AI.AI_State.shooting)
        {
            flash.transform.LookAt(npcSight.targetSightingPosition);
        }
        else
        {
            flash.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }

    // Überprüft, ob der Spieler im Lampenlicht steht
    bool CheckPlayerIllumination()
    {
        Vector3 direciton = playerHead.transform.position - flash.transform.position;
        float angle = Vector3.Angle(direciton, flash.transform.forward);
        if (angle < lightAngle / 2)
            return true;
        return false;
    }
}
