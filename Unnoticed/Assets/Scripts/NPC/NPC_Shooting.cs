﻿using UnityEngine;
using System.Collections;
using System;

public class NPC_Shooting : MonoBehaviour {

    public GameObject npcHead;
    public float maxDamage = 40f;
    public float minDamage = 5f;
    public float maxDeviation = 0.5f;              
    public AudioClip shotClip;                          
    public float flashIntensity = 3f;                   
    public float fadeSpeed = 10f;                       

    private Animator anim;                              
    private HashIDs hash;
    private GameObject flash;                           
    private LineRenderer bulletTrail;                 
    private Light shotLight;                       
    private SphereCollider col;                         
    private GameObject player;
    private GameObject playerHead;                     
    private Player_Health playerHealth;                 
    private bool shooting;

    private Vector3 target;
    private float scaledDamage;

    void Awake()
    {
        #region Zuweisung der Variablen
        anim = GetComponent<Animator>();
        bulletTrail = GetComponentInChildren<LineRenderer>();
        flash = bulletTrail.gameObject;
        shotLight = flash.GetComponent<Light>();
        col = GetComponent<SphereCollider>();
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerHead = GameObject.FindGameObjectWithTag(Tags.playerHead);
        playerHealth = player.gameObject.GetComponent<Player_Health>();
        hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
        
        target = Vector3.zero;
        scaledDamage = maxDamage - minDamage;
        #endregion

        bulletTrail.enabled = false;
        shotLight.intensity = 0f;
    }

    void Update()
    {
        // Speichtert den aktuellen Wert der Schusskurve
        float shot = anim.GetFloat(hash.shotFloat);
        
        // Für den Fall dass die Schusskurve an eine Spitze kommt und der Gegner im Moment nicht schießt
        if (shot > 0.5f && !shooting)
            Shoot();
        
        if (shot < 0.5f)
        {
            shooting = false;
            bulletTrail.enabled = false;
        }

        shotLight.intensity = Mathf.Lerp(shotLight.intensity, 0f, fadeSpeed * Time.deltaTime);
    }

    // Stellt die Animation um, je nach KI Zustand
    void OnAnimatorIK(int layerIndex)
    {
        // Speichere den aktuellen Wert des AimWeight Graphen
        float aimWeight = anim.GetFloat(hash.aimWeightFloat);

        // Setze die IK position der rechten Hand auf den Kopf des Spielers
        anim.SetIKPosition(AvatarIKGoal.RightHand, playerHead.transform.position);

        // Setze das Gewicht der IK 
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, aimWeight);
    }

    /* Lässt den Gegner schießen
    * Berechnet einen Genauigkeitswert je nach Abstand zum Ziel
    * Berechnet einen zufälligen Zielvektor anhand des Genauigkeitswerts und einer maximalen Abweichung
    * ausgehend vom Kopf des Spielers als Ziel
    * Berechnet anschließend die Richtung des Schussvektors ausgehend vom Lauf der Waffe
    * Falls ein Raycast mit der berechneten Richtung auf den Spieler trifft wird der Schaden berechnet
    * Je näher der Treffer am Kopf oder am Zentrum vom Spieler ist, desto mehr Schaden nimmt dieser
    * Falls der Raycast nicht trifft, so wird der Zielpunkt um ein Vielfaches in die gleiche Richtung verlegt
    * Anschließend wird ein Effekt für den Schuss aktiviert
    */ 
    void Shoot()
    {
        shooting = true;

        float accuracy = 1.0f;

        float distance = Vector3.Distance(npcHead.transform.position, playerHead.transform.position);
        accuracy = 1 - distance / (col.radius * 0.8f);
        float deviationScale = 1 - accuracy;

        target = playerHead.transform.position;

        Vector3 horizontalDeviation = npcHead.transform.right * UnityEngine.Random.Range(-maxDeviation * deviationScale, maxDeviation * deviationScale);
        Vector3 verticalDeviation = npcHead.transform.up * UnityEngine.Random.Range(-maxDeviation * deviationScale, maxDeviation * deviationScale);
        
        target += horizontalDeviation + verticalDeviation;
        Vector3 direction = target - flash.transform.position;
        
        RaycastHit hit;
        if(Physics.Raycast(flash.transform.position, direction.normalized, out hit, col.radius))
        {
            if(hit.collider.gameObject == player)
            {
                float centerDistance = Vector3.Distance(player.transform.position, hit.point);
                float headDistance = Vector3.Distance(playerHead.transform.position, hit.point);
                float distanceScale = Mathf.Min(centerDistance, headDistance);
                
                playerHealth.TakeDamage(minDamage + scaledDamage * distanceScale);
            }
            target = hit.point;
        }
        else
        {
            target = Vector3.Project(target, direction) * 5f;
        }
        PlayerTracker.ShotFired(flash.transform.position, 0f);
        ShotEffects(target);
    }

    // Führt einen Effekt beim Feuern der Waffe aus
    void ShotEffects(Vector3 target)
    {
        bulletTrail.SetPosition(0, bulletTrail.transform.position);
        bulletTrail.SetPosition(1, target);
        bulletTrail.enabled = true;
        shotLight.intensity = flashIntensity;
        AudioSource.PlayClipAtPoint(shotClip, shotLight.transform.position);
    }
}
