﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
public class NPC_Sight : MonoBehaviour
{
    public GameObject npcHead;
    public float fieldOfViewAngle = 160f;    
    [HideInInspector]       
    public bool playerInSight = false;
    [HideInInspector]
    public float playerInSightTime = 0f;
    [HideInInspector]                
    public Vector3 targetSightingPosition;
    
    public AnimationCurve multiplierCurve = new AnimationCurve(new Keyframe(0f, 0f, 0f, 0f), new Keyframe(0.5f, 0.1f, 0.3f, 0.3f), new Keyframe(1f, 1f, 3.5f, 0f));   
                      
    private SphereCollider col;    
    private GameObject player;
    private Rigidbody playerRigid;
    private Player_Movement playerMovement;
    private Player_Health playerHealth;
    private PlayerTracker playerTracker;
    private NPC_AI npcAI;
    private NPC_Flashlight flashLight;
    
    void Awake()
    {
        // Zuweisung der Variablen
        #region
        col = GetComponent<SphereCollider>();
        playerTracker = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<PlayerTracker>();
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerRigid = player.GetComponent<Rigidbody>();
        playerMovement = player.GetComponent<Player_Movement>();
        playerHealth = player.GetComponent<Player_Health>();
        npcAI = GetComponent<NPC_AI>();
        flashLight = GetComponentInChildren<NPC_Flashlight>();
        #endregion
        
        targetSightingPosition = playerTracker.resetPosition;
    }

    void Update()
    {
        if (npcAI.alert > npcAI.breakpointLooking && !playerMovement.Crouching && playerRigid.velocity.magnitude > 0f)
            targetSightingPosition = player.transform.position;

        if (playerInSight)
            targetSightingPosition = player.transform.position;
    }
    
    // Überprüft ob der Spieler in Sicht ist
    // Stellt den Alarmfaktor dementsprechend um
    // Je näher der Spieler zum Gegner steht und je näher der Spieler zur Mitte des Blickfeldes ist,
    // desto leichter ist er zu entdecken
    // Wenn der Spieler im Taschenlampenlicht steht, ist dieser noch einfach zu sehen
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInSight = false;

            if (playerHealth.health <= 0.0f)
                return;

            Vector3 playerPos = player.transform.position;

            if (playerMovement.Crouching)
                playerPos.y -= 0.19f;

            Vector3 direction = playerPos - npcHead.transform.position;
            
            float angle = Vector3.Angle(direction, npcHead.transform.forward);
            if (angle < fieldOfViewAngle * 0.5f)
            {
                RaycastHit hit;
                if (Physics.Raycast(npcHead.transform.position, direction.normalized, out hit, col.radius))
                {
                    if (hit.collider.gameObject == player)
                    {
                        playerInSight = true;
                        playerInSightTime += Time.deltaTime;

                        float angleRatio = 1 - (angle / (fieldOfViewAngle * 0.5f));
                        float distanceRatio = 1 - Vector3.Distance(transform.position, other.transform.position) / col.radius;

                        if (flashLight.playerIlluminated)
                            angleRatio *= 1 + distanceRatio;

                        float ratio = Mathf.Max(distanceRatio, angleRatio);
                        float baseMultiplier = PlayerTracker.playerVisibilityBase * multiplierCurve.Evaluate(ratio);

                        npcAI.alertScale = baseMultiplier * playerInSightTime;
                    }
                }
            }
            else
            {
                npcAI.alertScale = 0f;
                playerInSightTime = 0f;
            }
        }
    }
    
    // Setzt die Variablen zurück sobald der Spieler außer Sichtweite ist
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player) {
            playerInSight = false;
            npcAI.alertScale = 0f;
            playerInSightTime = 0f;
        }
    }
}
