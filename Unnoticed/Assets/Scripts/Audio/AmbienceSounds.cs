﻿using UnityEngine;
using System.Collections;

// Zuständig für die Hintergrundmusik in der Szene
public class AmbienceSounds : MonoBehaviour {

    private AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Stellt die Lautstärke der Geräusche auf 0 falls das Spiel pausiert ist
	void Update()
    {
        if (Time.deltaTime > 0)
            source.volume = 1f;
        else
            source.volume = 0f;
    }
}
