﻿using UnityEngine;
using System.Collections;
using System;

public class Player_Movement : MonoBehaviour
{
    [Header("Collider settings")]
    public float colliderStandHeight = 1.88f;
    public float colliderCrouchHeight = 1.5f;
    public float colliderRadius = 0.5f;

    [Header("Movement settings")]
    public float maximumNormalSpeed = 5.0f;
    private float ForwardSpeed;
    private float StrafeSpeed;
    private float BackwardsSped;
    public float RunMultiplier = 1.5f;
    public float CrouchMultiplier = 0.6f;
    public float JumpForce = 50f;
    public AnimationCurve SlopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));
    
    public float groundCheckDistance = 0.01f; 
    public float stickToGroundHelperDistance = 0.5f; 
    public bool airControl;

    [Header("Mouse settings")]
    public float sensiivityHorizontal = 2f;
    public float sensitivityVertical = 2f;
    public bool clampVerticalRotation = true;
    public float minimumHorizontalAngle = -90F;
    public float maximumHorizontalAngle = 90F;
    public bool smoothHeadMovement = true;
    [Range(1f, 10f)]
    public float smoothFactor = 8f;

    private Quaternion characterTargetRotation;
    private Quaternion cameraTargetRotation;

    [Header("Audio Assets")]
    public AudioClip[] jumpSounds;

    private GameObject playerHead;
    private GameObject playerViewport;
    private Player_Input playerInput;
    private Rigidbody rigid;
    private CapsuleCollider col;

    private float CurrentTargetSpeed = 8f;
    private float yRotation;
    private Vector3 groundContactNormal;
    private bool jump, previouslyGrounded, jumping, isGrounded, running, crouching;
    
    void Awake()
    {
        playerHead = GameObject.FindGameObjectWithTag(Tags.playerHead);
        playerViewport = GameObject.FindGameObjectWithTag(Tags.playerViewport);
        playerInput = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player_Input>();
        rigid = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

        ForwardSpeed = maximumNormalSpeed;
        StrafeSpeed = maximumNormalSpeed * 0.8f;
        BackwardsSped = maximumNormalSpeed * 0.8f;
        
        characterTargetRotation = transform.localRotation;
        cameraTargetRotation = playerViewport.transform.localRotation;
    }

    void Update()
    {
        RotateView(transform, playerViewport.transform);

        if (Input.GetKeyDown(playerInput.jumpKey) && !jumping)
        {
            AudioSource.PlayClipAtPoint(jumpSounds[UnityEngine.Random.Range(0, jumpSounds.Length)], transform.position, crouching ? 0.5f : 1f);
            jump = true;
        }

        if (Input.GetKeyDown(playerInput.crouchKeyD) || Input.GetKeyDown(playerInput.crouchKeyL))
        {
            Crouch();
        }
    }

    void FixedUpdate()
    {
        // Movement part
        GroundCheck();
        Vector2 input = Player_Input.GetPlayerMovementInput();

        UpdateDesiredTargetSpeed(input);

        Vector3 desiredMove = playerViewport.transform.forward * input.y + playerViewport.transform.right * input.x;
        desiredMove = Vector3.ProjectOnPlane(desiredMove, groundContactNormal).normalized;

        desiredMove.x = desiredMove.x * CurrentTargetSpeed;
        desiredMove.y = desiredMove.y * CurrentTargetSpeed;
        desiredMove.z = desiredMove.z * CurrentTargetSpeed;
        
        if (rigid.velocity.sqrMagnitude < (CurrentTargetSpeed * CurrentTargetSpeed))
        {
            rigid.AddForce(desiredMove, ForceMode.Impulse);
        }

        if (crouching)
        {
            rigid.drag = 4.0f;
        }

        // Springen/Kriechen Teil
        if (isGrounded)
        {
            rigid.drag = 4f;

            if (jump)
            {
                rigid.drag = 0f;
                rigid.velocity = new Vector3(rigid.velocity.x, 0f, rigid.velocity.z);
                rigid.AddForce(new Vector3(0f, crouching ? JumpForce * 0.8f : JumpForce, 0f), ForceMode.Impulse);
                jumping = true;
            }

            // Check if game is paused effectively so the rigidbody can be stopped
            if (!jumping && Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon && rigid.velocity.magnitude < 1f)
            {
                rigid.Sleep();
            }
        }
        else
        {
            rigid.drag = 0f;
            if (previouslyGrounded && !jumping)
            {
                StickToGroundHelper();
            }
        }
        jump = false;
    }
    
    // Behält den Spieler am Boden für einen unvorhergesehen Fall dass die isGrounded Variable
    // nicht korrekt festgelegt wird
    private void StickToGroundHelper()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, colliderRadius, Vector3.down, out hitInfo, colliderStandHeight / 2f - colliderRadius + stickToGroundHelperDistance)) 
        {
            if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
            {
                rigid.velocity = Vector3.ProjectOnPlane(rigid.velocity, hitInfo.normal);
            }
        }
    }

    // Überprüft, ob der Spieler auf etwas steht
    private void GroundCheck()
    {
        previouslyGrounded = isGrounded;
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, colliderRadius, Vector3.down, out hitInfo, (colliderStandHeight / 2f - colliderRadius) + groundCheckDistance))
        {
            isGrounded = true;
            groundContactNormal = hitInfo.normal;
        }
        else
        {
            isGrounded = false;
            groundContactNormal = Vector3.up;
        }
        if (!previouslyGrounded && isGrounded && jumping)
        {
            jumping = false;
        }
    }

    // Stellt den Geschwindigkeitsvektor für die Rigidbody Komponente je nach Input um
    private void UpdateDesiredTargetSpeed(Vector2 input)
    {
        if (input == Vector2.zero) return;
        if (input.x > 0 || input.x < 0)
        {
            CurrentTargetSpeed = StrafeSpeed;
        }
        if (input.y < 0)
        {
            CurrentTargetSpeed = BackwardsSped;
        }
        if (input.y > 0)
        {
            CurrentTargetSpeed = ForwardSpeed;
        }
        if (crouching)
        {
            CurrentTargetSpeed *= CrouchMultiplier;
        }
        
        if ((Input.GetKey(playerInput.runKeyD) || Input.GetKey(playerInput.runKeyL)) && !jumping)
        {
            if (crouching)
            {
                Crouch();
                crouching = false;
            }
            CurrentTargetSpeed *= RunMultiplier;
            running = true;
        }
        else
        {
            running = false;
        }
    }

    // Ändert die Haltung des Spielers wenn man die entsprechende Taste drückt
    void Crouch()
    {
        StartCoroutine(playerHead.GetComponent<Head_Movement>().SmoothMove(crouching));
        crouching = !crouching;

        if (crouching)
        {
            col.height = colliderCrouchHeight;
            Vector3 newCenter = col.center;
            newCenter.y -= (colliderStandHeight - colliderCrouchHeight) / 2;
            col.center = newCenter;
        }
        else
        {
            col.center = Vector3.zero;
            col.height = colliderStandHeight;
        }
    }
    
    // Rotiert den "kopf" und den "Körper" des Spielers
    private void RotateView(Transform character, Transform camera)
    {
        // Verhindert dass man sich umsehen kann wenn das Spiel pausiert wird
        if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;
        float oldYRotation = transform.eulerAngles.y;

        float yRot = Input.GetAxis("Mouse X") * sensiivityHorizontal;
        float xRot = Input.GetAxis("Mouse Y") * sensitivityVertical;

        characterTargetRotation *= Quaternion.Euler(0f, yRot, 0f);
        cameraTargetRotation *= Quaternion.Euler(-xRot, 0f, 0f);

        if (clampVerticalRotation) cameraTargetRotation = ClampRotationAroundXAxis(cameraTargetRotation);

        if (smoothHeadMovement)
        {
            character.localRotation = Quaternion.Slerp(character.localRotation, characterTargetRotation, smoothFactor * Time.deltaTime);
            camera.localRotation = Quaternion.Slerp(camera.localRotation, cameraTargetRotation, smoothFactor * Time.deltaTime);
        }
        else
        {
            character.localRotation = characterTargetRotation;
            camera.localRotation = cameraTargetRotation;
        }

        if (isGrounded || airControl)
        {
            // Rotiert die Rigidbody Komponente entsprechend der Richtung in die der Spieler blickt
            Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
            rigid.velocity = velRotation * rigid.velocity;
        }
    }

    private Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX, minimumHorizontalAngle, maximumHorizontalAngle);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);
        return q;
    }

    public Vector3 Velocity
    {
        get { return rigid.velocity; }
    }

    public bool Grounded
    {
        get { return isGrounded; }
    }
    

    public bool Jumping
    {
        get { return jumping; }
    }

    public bool Running
    {
        get
        {
            return running;
        }
    }

    public bool Crouching
    {
        get
        {
            return crouching;
        }
    }
    
}
