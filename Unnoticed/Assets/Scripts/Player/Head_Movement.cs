﻿using UnityEngine;
using System.Collections;
using Toolbox;

public class Head_Movement : MonoBehaviour {
    
    private float smoothSpeed = 0.8f;
    private float standHeight = 0.6f;
    private float crouchHeight = 0.15f;
    
    private float shakeDuration = 0.25f;
    private float shakeMagnitude = 0.07f;
    
    // Stellt die Höhe des "Kopfes" vom Spieler je nach Haltung um
    public IEnumerator SmoothMove(bool up)
    {
        float t = 0.0f;
        while (t <= 1.0f)
        {
            t += Time.deltaTime / smoothSpeed;
            transform.localPosition = up ? 
                Vector3.Lerp(transform.localPosition, new Vector3(0, standHeight, 0), Mathf.SmoothStep(0.0f, 1.0f, t)) 
                : Vector3.Lerp(transform.localPosition, new Vector3(0, crouchHeight, 0), Mathf.SmoothStep(0.0f, 1.0f, t));
            yield return new WaitForFixedUpdate();
        }
    }

    // Schüttelt den "Kopf" mit einer Perlin-Noise-Funktion
    public IEnumerator ShakeCamera()
    {
        float elapsed = 0.0f;

        Vector3 originalPos = transform.localPosition;

        while (elapsed < shakeDuration)
        {
            elapsed += Time.deltaTime;

            float percentComplete = elapsed / shakeDuration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= shakeMagnitude * damper;
            y *= shakeMagnitude * damper;

            transform.localPosition = new Vector3(x, originalPos.y + y, originalPos.z);

            yield return new WaitForFixedUpdate();
        }

        transform.localPosition = originalPos;
    }
}
