﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using UnityStandardAssets.ImageEffects;

public class Player_Input : MonoBehaviour
{
    public KeyCode useKeyD = KeyCode.E;
    public KeyCode useKeyL = KeyCode.Keypad0;
    public KeyCode flashlightKeyD = KeyCode.F;
    public KeyCode flashlightKeyL = KeyCode.Keypad1;
    public KeyCode runKeyD = KeyCode.LeftShift;
    public KeyCode runKeyL = KeyCode.RightShift;
    public KeyCode crouchKeyD = KeyCode.C;
    public KeyCode crouchKeyL = KeyCode.RightControl;
    public KeyCode pauseKey = KeyCode.Escape;
    public KeyCode jumpKey = KeyCode.Space;
    
    [Range(1f, 10f)]
    public float useDistance = 3f;

    public static Vector3 playerMovementInput;

    private Camera viewport;
    private Light flashLight;
    private AudioAssets audioAssets;
    private MainLevelController levelController;
    
    void Awake()
    {
        playerMovementInput = new Vector3();
        viewport = GetComponentInChildren<Camera>();
        flashLight = GetComponent<Light>();
        audioAssets = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<AudioAssets>();
        try
        {
            levelController = GameObject.FindGameObjectWithTag(Tags.levelController).GetComponent<MainLevelController>();
        }catch(Exception e) {
            Debug.Log(e);
        } 
    }

    void Start()
    {

    }
    
    // Überprüft den Input für Bewegungen von der Tastatur
    void FixedUpdate()
    {
        playerMovementInput.x = Input.GetAxis("Horizontal");
        playerMovementInput.y = Input.GetAxis("Vertical");
    }
    
    void Update()
    {
        CheckForUseInput();
        CheckLighter();
    }

    // Überprüft ob die "nutzen" Taste gedrückt wurde und führt je nach Tag entsprechende Methoden aus
    void CheckForUseInput()
    {
        if (Input.GetKeyDown(useKeyD) || Input.GetKeyDown(useKeyL))
        {
            RaycastHit hit;
            if (Physics.Raycast(viewport.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2)), out hit, useDistance))
            {
                GameObject hitThing = hit.collider.gameObject;
                string tag = hitThing.tag;
                switch (tag)
                {
                    case Tags.door:
                        try
                        {
                            hitThing.GetComponentInParent<Door>().TurnDoor(transform.position);
                        }catch(Exception e)
                        {
                            Debug.Log(e);
                        }
                        break;
                    case Tags.item:
                        AudioSource.PlayClipAtPoint(audioAssets.misc[4], hit.point, 1f);
                        GameObject[] items = GameObject.FindGameObjectsWithTag(Tags.item);
                        for (int i = 0; i < items.Length; i++)
                            Destroy(items[i]);
                        levelController.ItemCollected();
                        break;
                    case Tags.delivery:
                        if (levelController.itemCollected)
                        {
                            if (hitThing.GetComponentInParent<Delivery>().Glow)
                            {
                                AudioSource.PlayClipAtPoint(audioAssets.misc[3], hit.point, 1f);
                                hitThing.GetComponentInParent<Delivery>().Glow = false;
                                levelController.deliveriesMarked++;
                            }
                        }
                        break;
                    case Tags.gate:
                        if (levelController.itemCollected && levelController.deliveriesMarked > 1)
                        {
                            AudioSource.PlayClipAtPoint(audioAssets.misc[2], hit.point, 0.5f);
                            levelController.WinGame();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    // Stellt die Helligkeit für das Feuerzeug je nach Input und Status um
    void CheckLighter()
    {
        if (Input.GetKeyDown(flashlightKeyD) || Input.GetKeyDown(flashlightKeyL))
            StartCoroutine(UseLighter());
        
        if (flashLight.enabled)
            PlayerTracker.playerVisibilityBase = 30f;
        else
            PlayerTracker.playerVisibilityBase = 10f;

        flashLight.intensity = Mathf.Lerp(flashLight.intensity, UnityEngine.Random.Range(0.1f, 1), 0.03f);
    }

    // Aktiviert/Deaktiviert das Feuerzeug und spielt passende AudioClips ab
    IEnumerator UseLighter()
    {
        if (!flashLight.enabled)
        {
            AudioSource.PlayClipAtPoint(audioAssets.zippo[0], transform.position, 0.5f);
            yield return new WaitForSeconds(audioAssets.zippo[0].length * 1.5f);
            AudioClip lightSound = audioAssets.lighter[UnityEngine.Random.Range(0, 2)];
            AudioSource.PlayClipAtPoint(lightSound, transform.position, 0.5f);
            yield return new WaitForSeconds(lightSound.length);
        }
        else
            AudioSource.PlayClipAtPoint(audioAssets.zippo[1], transform.position, 0.5f);

        flashLight.enabled = !flashLight.enabled;
        yield return null;
    }
    
    public static Vector3 GetPlayerMovementInput()
    {
        return playerMovementInput;
    }
}