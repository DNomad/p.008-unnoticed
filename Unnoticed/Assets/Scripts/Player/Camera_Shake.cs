﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Camera))]
public class Camera_Shake : MonoBehaviour
{
    public float StrideInterval = 8f;
    [Range(0f, 1f)]
    public float RunningStrideLengthen = 0.8f;
    [Range(0f, 3f)]
    public float CrouchingStrideLengthen = 0.9f;
    public float RunningShakeHeight = 0.03f;
    public float CrouchingShakeHeight = 0.02f;
    public float defaultShakeHeight = 0.04f;

    [Header("Audio Clips")]
    public AudioClip landSoundSilent;
    public AudioClip landSoundNormal;
    public AudioClip landSoundHard;
    public AudioClip stepClip;
    private float hardLandTreshold = -5f;
    private float normalLandTreshold = -1f;
    private bool firstShot;
    private float maxFallVelocity = 0f;
    
    public CurveControlledShake motionShake;
    public LerpControlledShake jumpAndLandingShake;

    private Camera cam;
    private GameObject player;
    private Player_Movement playerMovement;
    private bool previouslyGrounded;
    private Vector3 originalCameraPosition;
    private Rigidbody rigid;

    private void Awake()
    {
        // Zuweisung der Variablen
        #region
        cam = GetComponent<Camera>();
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerMovement = player.GetComponent<Player_Movement>();
        motionShake = new CurveControlledShake();
        jumpAndLandingShake = new LerpControlledShake();
        motionShake.Setup(cam, StrideInterval);
        originalCameraPosition = cam.transform.localPosition;
        rigid = player.GetComponent<Rigidbody>();
        firstShot = true;
        #endregion
    }

    // Erzeugt einen Wackeleffekt beim Gehen/Rennen/Kriechen
    // Spielt außerdem Schrittgeräusche ab
    private void Update()
    {
        maxFallVelocity = Mathf.Min(rigid.velocity.y, maxFallVelocity);
        Vector3 newCameraPosition;
        if (playerMovement.Velocity.magnitude > 0 && playerMovement.Grounded)
        {
            cam.transform.localPosition = motionShake.DoHeadShake(
                playerMovement.Velocity.magnitude * (playerMovement.Running ? RunningStrideLengthen : (playerMovement.Crouching ? CrouchingStrideLengthen : 1f)), 
                playerMovement.Running ? RunningShakeHeight : (playerMovement.Crouching ? CrouchingShakeHeight : defaultShakeHeight));
            newCameraPosition = cam.transform.localPosition;
            newCameraPosition.y = cam.transform.localPosition.y - jumpAndLandingShake.Offset();

            if (motionShake.CurveValue < -1.40f)
            {
                if (firstShot)
                {
                    AudioSource.PlayClipAtPoint(stepClip, player.transform.position, rigid.velocity.magnitude / 20f);
                    firstShot = false;
                    if (!playerMovement.Crouching)
                    {
                        try
                        {
                            PlayerTracker.Stepped(player.transform.position, 0);
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                }
            }
            else
            {
                firstShot = true;
            }
        }
        else
        {
            newCameraPosition = cam.transform.localPosition;
            newCameraPosition.y = originalCameraPosition.y - jumpAndLandingShake.Offset();
        }
        cam.transform.localPosition = newCameraPosition;

        if (!previouslyGrounded && playerMovement.Grounded)
        {
            if (playerMovement.Crouching)
                AudioSource.PlayClipAtPoint(landSoundSilent, transform.position, Mathf.Clamp(maxFallVelocity / -5f, 0f, 1f));
            else
            {
                if(maxFallVelocity < hardLandTreshold)
                    AudioSource.PlayClipAtPoint(landSoundHard, transform.position, 1f);
                else if(maxFallVelocity < normalLandTreshold)
                    AudioSource.PlayClipAtPoint(landSoundNormal, transform.position, Mathf.Clamp(maxFallVelocity / -3f, 0f, 1f));
                else 
                    AudioSource.PlayClipAtPoint(landSoundSilent, transform.position, Mathf.Clamp(maxFallVelocity / -1f, 0f, 1f));
            }
            
            try
            {
                PlayerTracker.Jumped(player.transform.position, Mathf.Abs(maxFallVelocity));
            }catch(Exception e)
            {
                e.ToString();
            }
            
            StartCoroutine(jumpAndLandingShake.DoShakeCycle());
            maxFallVelocity = 0f;
        }

        previouslyGrounded = playerMovement.Grounded;
    }

    // Erzeugt den Wackeleffekt für den Kopf
    [System.Serializable]
    public class CurveControlledShake
    {
        public float HorizontalShakeRange = 0.05f;
        public AnimationCurve Shakecurve = new AnimationCurve(
            new Keyframe(0f, 0f), new Keyframe(0.125f, 1.5f), 
            new Keyframe(0.375f, -1.5f), new Keyframe(0.5f, 0f));

        public float VerticalToHorizontalRatio = 2f;

        private float cyclePositionX;
        private float cyclePositionY;
        private float ShakeBaseInterval;
        private Vector3 originalCameraPosition;
        private float time;
        
        public void Setup(Camera camera, float ShakeBaseInterval)
        {
            this.ShakeBaseInterval = ShakeBaseInterval;
            originalCameraPosition = camera.transform.localPosition;
            
            time = Shakecurve[Shakecurve.length - 1].time;
        }
        
        public Vector3 DoHeadShake(float speed, float height)
        {
            float xPos = originalCameraPosition.x + (Shakecurve.Evaluate(cyclePositionX) * HorizontalShakeRange);
            float yPos = originalCameraPosition.y + (Shakecurve.Evaluate(cyclePositionY) * height);
            
            cyclePositionX += (speed * Time.deltaTime) / ShakeBaseInterval;
            cyclePositionY += ((speed * Time.deltaTime) / ShakeBaseInterval) * VerticalToHorizontalRatio;

            if (cyclePositionX > time)
            {
                cyclePositionX = cyclePositionX - time;
            }
            if (cyclePositionY > time)
            {
                cyclePositionY = cyclePositionY - time;
            }
            return new Vector3(xPos, yPos, 0f);
        }

        public float CurveValue
        {
            get
            {
                return Shakecurve.Evaluate(cyclePositionY);
            }
        }
    }

    // Erzeugt einen kurzen Effekt beim Landen
    [System.Serializable]
    public class LerpControlledShake
    {
        public float shakeDuration = 0.2f;
        public float shakeAmount = 0.2f;

        private float offset = 0f;
        
        public float Offset()
        {
            return offset;
        }

        public IEnumerator DoShakeCycle()
        {
            // Bewegt die Kamera ein wenig nach unten
            float t = 0f;
            while (t < shakeDuration)
            {
                offset = Mathf.Lerp(0f, shakeAmount, t / shakeDuration);
                t += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }

            // Stellt die Kamera langsam auf die ursprüngliche Höhe
            t = 0f;
            while (t < shakeDuration)
            {
                offset = Mathf.Lerp(shakeAmount, 0f, t / shakeDuration);
                t += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            offset = 0f;
        }
    }
}
