﻿using UnityEngine;
using System.Collections;

// Aktiviert/Deaktiviert je nach Zustand des Spiels den Cursor
// Zeichnet außerdem ein Fadenkreuz auf dem Bildschirm
public class Camera_Cursor : MonoBehaviour {

    protected bool cursorVisible = false;
    public Texture2D crosshair;
    private Rect position;
    public float scaleFactor = 0.2f;

	void Start () {
        position = new Rect
            ((Screen.width - crosshair.width * scaleFactor) / 2, 
            (Screen.height - crosshair.height * scaleFactor)/2, 
            crosshair.width * scaleFactor, 
            crosshair.height * scaleFactor);
        if (cursorVisible)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    void OnGUI()
    {
        GUI.DrawTexture(position, crosshair);
    }
}
