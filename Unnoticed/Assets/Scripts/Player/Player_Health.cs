﻿using UnityEngine;
using System.Collections;

public class Player_Health : MonoBehaviour
{
    public float maxHealth = 100.0f;
    public float regenWaitTime = 6f;
    public float health;
    public bool regenerating = true;
    public bool infinite = false;

    private float heartbeatTriggerPoint = 0.5f;
    private Rigidbody rigid;
    private float timer = 0f;
    private AudioSource heartBeat;
    private float deathForceScale = 2.0f;
    private PlayerTracker playerTracker;

    void Awake()
    {
        // Zuweisung der Variablen
        #region
        health = maxHealth;
        rigid = GetComponent<Rigidbody>();
        heartBeat = GetComponent<AudioSource>();
        playerTracker = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<PlayerTracker>();
        #endregion
    }

    void Update()
    {
        if (health <= 0)
            Die();

        if(infinite)
            health = 100f;
        
        if (regenerating && timer == 0f && health > 0)
        {
            health += health * Time.deltaTime * 0.1f;
            health = Mathf.Clamp(health, 0f, maxHealth);
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            timer = Mathf.Clamp(timer, 0f, regenWaitTime);
        }

        UpdateHeartBeat();
    }

    // Stellt die Lautstärke des Herzschlags je nach Höhe der HP um
    void UpdateHeartBeat()
    {
        if(health <= maxHealth * heartbeatTriggerPoint && health > 0f)
        {
            float ratio = 1 - health / (maxHealth * heartbeatTriggerPoint);
            heartBeat.volume = ratio;
            heartBeat.pitch = 0.5f + 0.5f * ratio;
        }
        else
        {
            heartBeat.volume = 0f;
        }
    }
    
    // Lässt den Spieler Schaden nehmen 
    public void TakeDamage(float amount)
    {
        timer = amount / 40f * regenWaitTime;
        health -= amount;
        StartCoroutine(GetComponentInChildren<Head_Movement>().ShakeCamera());
    }

    // Stellt die entsprechenden Einstellugnen um, falls der Spieler stirbt
    private void Die()
    {
        GetComponent<Player_Movement>().enabled = false;
        GetComponentInChildren<Camera_Shake>().enabled = false;
        rigid.constraints = RigidbodyConstraints.None;
        rigid.constraints = RigidbodyConstraints.FreezeRotationY;
        if (rigid.velocity.magnitude < 0.5f)
            rigid.AddForce(new Vector3(Random.value * deathForceScale, Random.value * deathForceScale, Random.value * deathForceScale), ForceMode.Impulse);

        rigid.mass = 80.0f;

        heartBeat.volume = 0f;

        playerTracker.globalLastKnownPosition = playerTracker.resetPosition;
    }

    // Stellt die Starteinstellungen für den Spieler wieder her
    public void ResetPlayer()
    {
        health = maxHealth;
        GetComponent<Player_Movement>().enabled = true;
        GetComponentInChildren<Camera_Shake>().enabled = true;
        rigid.constraints = RigidbodyConstraints.FreezeRotation;
        rigid.mass = 10f;
        rigid.Sleep();
    }
}
