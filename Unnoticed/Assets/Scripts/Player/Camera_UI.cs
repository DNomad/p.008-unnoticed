﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Camera_UI : MonoBehaviour
{
    public Image vignetteImage;
    public Image faderImage;
    public float redLerpSpeeed = 0.1f;
    public float blackLerpSpeed = 1f;
    [HideInInspector]
    public bool fadeToBlack = false;
    [HideInInspector]
    public Color blackForegorundColor = Color.black;

    private float vignetteTriggerPoint = 0.7f;
    private Color vignetteColor = Color.red;
    private GameObject player;
    private Player_Health playerHealth;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerHealth = player.GetComponent<Player_Health>();
    }

    void Update()
    {
        #region Berechnet den Alpha-Wert der roten Vignette
        if (playerHealth.health < playerHealth.maxHealth * vignetteTriggerPoint && playerHealth.health > 0)
        {
            float ratio = 1 - playerHealth.health / (playerHealth.maxHealth * vignetteTriggerPoint);
            vignetteColor.a = Mathf.Lerp(vignetteColor.a, ratio, redLerpSpeeed);
        }
        else if (playerHealth.health <= 0)
        {
            vignetteColor.a = 1f;
        }
        else {
            vignetteColor.a = 0f;
        }
        #endregion
        
        #region Berechnet den Alpha-Wert der schwarzen Fläche
        if (fadeToBlack)
            blackForegorundColor.a = Mathf.Lerp(blackForegorundColor.a, 1f, blackLerpSpeed * Time.deltaTime);
        else
            blackForegorundColor.a = Mathf.Lerp(blackForegorundColor.a, 0f, blackLerpSpeed * Time.deltaTime);
        #endregion

        #region Setzt die entsprechenden Farben
        try
        {
            vignetteImage.color = vignetteColor;
            faderImage.color = blackForegorundColor;
        }catch(Exception e)
        {
            Debug.Log(e);
        }
        #endregion
    }
}