﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(NavMeshAgent))]
public class SVI_Event : HighlighterController {

    public float initialSoundDistance = 20.0f;
    public float minimumNoticableDistance = 30f;

    #region private Variablen
    private int eventID;
    private float soundDistance;
    private float sviTransparency = 0.0f;
    private float directDistanceFromPlayer;
    private AudioSource source;
    private NavMeshAgent nav;
    private GameObject player;
    private Player_Movement playerMovement;
    private SVI_I ProtagonistSVISystem;
    private GameObject gameController;
    private GameStats gameStats;
    #endregion

    private enum EventStatus
    {
        running, starting, inactive, stopping
    }
    private EventStatus currentStatus;

    new void Awake()
    {
        base.Awake();
        Init();
    }

    new void Update()
    {
        base.Update();
        UpdateTransparency();
        UpdateVolume();
        directDistanceFromPlayer = Toolbox.Spatial.CalcDistance(transform.position, player.transform.position);
        UpdateEventStatus();
        UpdateSVIValues();
    }

    // Initialisiert alle Variablen
    void Init()
    {
        gameController = GameObject.FindGameObjectWithTag(Tags.gameController);
        gameStats = gameController.GetComponent<GameStats>();

        eventID = -1;

        source = GetComponent<AudioSource>();
        soundDistance = initialSoundDistance;
        source.maxDistance = soundDistance;
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerMovement = player.GetComponent<Player_Movement>();
        ProtagonistSVISystem = player.GetComponent<SVI_I>();

        directDistanceFromPlayer = 0f;
        currentStatus = EventStatus.inactive;
    }

    // Berechnet die Effekttransparenz abhängig von der Haltung des Spielers
    void UpdateTransparency()
    {
        if (gameStats.SVIDirect)
        {
            if (playerMovement.Crouching)
                sviTransparency = Mathf.Lerp(sviTransparency, 1f, 0.1f);
            else
                sviTransparency = Mathf.Lerp(sviTransparency, 0f, 0.1f);
        }
        else
            sviTransparency = 0f;
    }

    // Stellt fest, welchen Status das Event hat je nach Reichweite zum Spieler
    private void UpdateEventStatus()
    {
        if (directDistanceFromPlayer <= soundDistance)
        {
            if (currentStatus == EventStatus.inactive)
                currentStatus = EventStatus.starting;
            else if (currentStatus == EventStatus.starting)
                currentStatus = EventStatus.running;
        }
        else
        {
            if (currentStatus == EventStatus.running)
                currentStatus = EventStatus.stopping;
            else if (currentStatus == EventStatus.stopping)
                currentStatus = EventStatus.inactive;
        }
    }

    // Berechnet den aktuellen Wert für die Transparenz des Effekts
    // Registiert / Entfernt das Event bei der indirekten Version
    private void UpdateSVIValues()
    {
        Color col = Color.white;
        if (currentStatus == EventStatus.inactive)
            col.a = 0f;
        else if(currentStatus == EventStatus.starting)
        {
            if (gameStats.SVIIndirect)
                eventID = ProtagonistSVISystem.SetupEvent(transform.position, soundDistance, eventID);
            if (gameStats.SVIDirect)
                col.a = (1 - (directDistanceFromPlayer / soundDistance)) * sviTransparency;
        }else if(currentStatus == EventStatus.running)
        {
            if (gameStats.SVIIndirect)
            {
                ProtagonistSVISystem.SetEventPosition(eventID, transform.position);
                ProtagonistSVISystem.SetEventDistance(eventID, soundDistance);
            }
            if (gameStats.SVIDirect)
                col.a = (1 - (directDistanceFromPlayer / soundDistance)) * sviTransparency;
        }else if(currentStatus == EventStatus.stopping)
        {
            if (gameStats.SVIIndirect)
                ProtagonistSVISystem.RemoveEvent(eventID);
            if (gameStats.SVIDirect)
                col.a = 0f;
        }
        if (!gameStats.SVIDirect)
            col.a = 0f;
        h.ConstantOnImmediate(col);
    }
    
    // Passt die Lautstärke für die Audioquelle je nach Abstand an
    private void UpdateVolume()
    {
        float pathDistance = Toolbox.Spatial.CalcPathDistance(transform.position, player.transform.position, ref nav);
        if (pathDistance - directDistanceFromPlayer > 1f)
        {
            float diff = pathDistance - directDistanceFromPlayer;
            source.volume = 1 - diff / minimumNoticableDistance;
        }
        else
        {
            source.volume = 1f;
        }
        source.maxDistance = soundDistance;
    }

    // Deaktiviert das Event und entfernt es vom SVI System
    void OnDisable()
    {
        ProtagonistSVISystem.RemoveEvent(eventID);
        currentStatus = EventStatus.inactive;
    }
}
