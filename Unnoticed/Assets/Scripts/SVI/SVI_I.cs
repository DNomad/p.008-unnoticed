﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[RequireComponent(typeof(LineRenderer))]
public class SVI_I : MonoBehaviour
{
    public Material lineMaterial;
    public bool WaveAffectsEventWave = true;
    public AnimationCurve EventSmoothCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0.8f, 1f), new Keyframe(1f, 1f));

    #region private Variablen
    private int lineRendererVertexCount = 500;
    private float DistanceFromCenter = 1.5f;
    private float CircleRelativeHeight = -0.6f;
    private float WaveSpeed = 0.5f;
    private float WaveFrequency = 0.15f;
    private float WaveMaxAlteredHeight = 0.01f;
    private float VertexMaximumAlteredHeight = 0.3f;
    private float EventMaxAngleGap = 30.0f;
    private float CircleAngles = 360.0f;
    private float sviTransparency = 0.0f;
    private int EventCount;
    private Dictionary<int, Vector3> EventPositionsList;
    private Dictionary<int, float> EventDistancesList;
    private GameObject gameController;
    private GameStats gameStats;
    private Player_Movement playerMovement;
    private LineRenderer lineRenderer;
    private Vector3[] lineRendererVertices;
    #endregion

    void Awake()
    {
        Init();
    }
    
    // Initialisiert alle Variablen
    void Init()
    {
        gameController = GameObject.FindGameObjectWithTag(Tags.gameController);
        gameStats = gameController.GetComponent<GameStats>();
        playerMovement = GetComponent<Player_Movement>();

        EventPositionsList = new Dictionary<int, Vector3>();
        EventDistancesList = new Dictionary<int, float>();
        EventCount = 0;
        SetupLineRenderer();
    }
    
    void Update()
    {
        UpdateTransparency();
        UpdateLineRenderer();
    }

    // Berechnet die Transparenz des Effektes abhängig von der Haltung des Spielers
    void UpdateTransparency()
    {
        if (gameStats.SVIIndirect)
        {
            if (playerMovement.Crouching)
                sviTransparency = Mathf.Lerp(sviTransparency, 1f, 0.1f);
            else
                sviTransparency = Mathf.Lerp(sviTransparency, 0f, 0.1f);
        }
        else
            sviTransparency = 0f;
    }

    void UpdateLineRenderer()
    {
        GetComponent<LineRenderer>().material.SetFloat("_Transparency", sviTransparency);
        AlignVerticesInCircle(transform.position, ref lineRendererVertices);
        AlignVerticesToEvents(transform.position, EventPositionsList, EventDistancesList, ref lineRendererVertices);
        AlignVerticesInWave(WaveMaxAlteredHeight, ref lineRendererVertices);
        lineRenderer.SetPositions(lineRendererVertices);
    }

    // Initialisiert die LineRenderer Komponente
    void SetupLineRenderer()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
        lineRendererVertices = new Vector3[lineRendererVertexCount];
        lineRenderer.material = lineMaterial;
        lineRenderer.SetVertexCount(lineRendererVertexCount);
        AlignVerticesInCircle(transform.position, ref lineRendererVertices);
        lineRenderer.SetPositions(lineRendererVertices);
    }

    // Ordnet das Vector3 Array in einem Kreis an
    // Richtet zusätzlich den Anfangs- und Endpunkt der LineRenderer Komponente konstant hinter den Spieler
    private void AlignVerticesInCircle(Vector3 center, ref Vector3[] vertices)
    {
        float vertexAngle = 90.0f + Toolbox.Spatial.CalcAngle(transform.position, transform.position + transform.forward, transform.forward, false);

        if (vertexAngle > CircleAngles)
            vertexAngle -= CircleAngles;
        else if (vertexAngle < CircleAngles)
            vertexAngle += CircleAngles;

        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 pos = center;
            pos.x += Mathf.Cos(vertexAngle * Mathf.Deg2Rad) * DistanceFromCenter;
            pos.z += Mathf.Sin(vertexAngle * Mathf.Deg2Rad) * DistanceFromCenter;
            pos.y += CircleRelativeHeight;
            vertices[i] = pos;
            vertexAngle += (CircleAngles / vertices.Length);

            if (vertexAngle > CircleAngles)
                vertexAngle -= CircleAngles;
            else if (vertexAngle < CircleAngles)
                vertexAngle += CircleAngles;
        }
    }

    /* Verformt die Linie aus Vertexen anhand der Liste mit registrierten SVI Events
    * Die Höhe der einzelnen Verformungen ist von der Distanz zum Event abhängig
    * Ein Event das näher am Spieler ist kann ein weiter entferntes Event überdecken
    */
    private void AlignVerticesToEvents(Vector3 center, Dictionary<int, Vector3> positions, Dictionary<int, float> maxDistances, ref Vector3[] vertices)
    {
        foreach (KeyValuePair<int, Vector3> pos in positions)
        {
            float angleScaleFactor = lineRendererVertexCount / CircleAngles;
            float angleToEvent = Toolbox.Spatial.CalcAngle(center, pos.Value, transform.right * -1, true) * angleScaleFactor;
            float distanceFromCenter = Toolbox.Spatial.CalcDistance(center, pos.Value);
            float distanceRatio = 1 - (distanceFromCenter / maxDistances[pos.Key]);
            float angleInterval = distanceRatio * EventMaxAngleGap * angleScaleFactor;

            int minIndex = Mathf.RoundToInt(angleToEvent - angleInterval);
            int maxIndex = Mathf.RoundToInt(angleToEvent + angleInterval);

            for (int i = minIndex; i < maxIndex; i++)
            {
                int j = i;
                float smoothedAngleRatio = 1 - EventSmoothCurve.Evaluate(Mathf.Abs(angleToEvent - j) / angleInterval);

                if (i >= lineRendererVertexCount)
                    j -= lineRendererVertexCount;
                else if (i < 0)
                    j += lineRendererVertexCount;

                if (vertices[j].y < (transform.position.y + CircleRelativeHeight + (VertexMaximumAlteredHeight * distanceRatio * smoothedAngleRatio)))
                    vertices[j].y = transform.position.y + CircleRelativeHeight + (VertexMaximumAlteredHeight * distanceRatio * smoothedAngleRatio);
            }
        }
    }

    /* Manipuliert die y-Werte der Vertexe so, dass ein Welleneffekt entsteht
    * Der Wellenefekt wird nicht von der Rotation des Spielers beeinflusst
    * Zusätzlich kann eingestellt werden, 
    * ob die Verformungen durch nahe Events vom Effekt beeinflusst werden sollen
    */
    private void AlignVerticesInWave(float height, ref Vector3[] vertices)
    {
        int vertexIndex = Mathf.RoundToInt(Toolbox.Spatial.CalcAngle(transform.position, transform.position + transform.forward, transform.forward * -1, false));
        int c = 0;
        for (int a = vertexIndex; a < vertexIndex + lineRendererVertexCount; a++)
        {
            int b = a;
            if (b >= lineRendererVertexCount)
                b -= lineRendererVertexCount;
            else if (a < 0)
                b += lineRendererVertexCount;

            if (WaveAffectsEventWave)
                vertices[c].y += Mathf.Sin(b * WaveFrequency + Time.time * WaveSpeed) * height;

            else
            {
                if (vertices[c].y <= transform.position.y + CircleRelativeHeight + Mathf.Abs(Mathf.Sin(b * WaveFrequency + Time.time * WaveSpeed) * height))
                    vertices[c].y += Mathf.Sin(b * WaveFrequency + Time.time * WaveSpeed) * height;
            }
            c++;
        }
    }

    // Registriert ein neues SVI Event
    public int SetupEvent(Vector3 position, float maxDistance, int id)
    {
        if (id < 0)
        {
            int newID = EventCount;
            EventCount++;
            EventPositionsList.Add(newID, position);
            EventDistancesList.Add(newID, maxDistance);
            return newID;
        }
        else
        {
            if (!(EventPositionsList.ContainsKey(id) && EventDistancesList.ContainsKey(id)))
            {
                EventPositionsList.Add(id, position);
                EventDistancesList.Add(id, maxDistance);
            }
            return id;
        }
    }

    // Aktualisiert die Position eines SVI Events
    public void SetEventPosition(int id, Vector3 position)
    {
        if (EventPositionsList.ContainsKey(id))
            EventPositionsList[id] = position;
    }


    // Aktualisiert die Distanz eines SVI Events
    public void SetEventDistance(int id, float distance)
    {
        if (EventDistancesList.ContainsKey(id))
            EventDistancesList[id] = distance;
    }

    // Entfernt ein SVI Event aus der Liste
    public void RemoveEvent(int index)
    {
        EventPositionsList.Remove(index);
        EventDistancesList.Remove(index);
    }
}
