﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

// LevelController für das Tutorial
public class TutorialController : MonoBehaviour {

    public Text missionText;

    [HideInInspector]
    public Vector3 spawnPosition;
    [HideInInspector]
    public Quaternion spawnRotation;

    private GameObject player;
    private Camera playerViewport;
    private Player_Health playerHealth;
    private Player_Input playerInput;
    private Camera_UI fader;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerViewport = GameObject.FindGameObjectWithTag(Tags.playerViewport).GetComponent<Camera>();
        playerHealth = player.GetComponent<Player_Health>();
        playerInput = player.GetComponent<Player_Input>();
        fader = player.GetComponentInChildren<Camera_UI>();
        spawnPosition = player.transform.position;
        spawnRotation = player.transform.rotation;
    }

    void Start()
    {
        fader.fadeToBlack = false;
    }

    void Update()
    {
        // Falls der Spieler stirbt
        if (playerHealth.health <= 0f)
        {
            StartCoroutine(RespawnPlayer());
        }

        // Überprüft ob die Tür am Ende des Levels betätigt wurde, damit das Tutorial beendet werden kann
        RaycastHit hit;
        if(Physics.Raycast(playerViewport.ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2)), out hit, playerInput.useDistance)){
            if(hit.collider.gameObject.tag == Tags.door)
            {
                if (Input.GetKeyDown(playerInput.useKeyD) || Input.GetKeyDown(playerInput.useKeyL)) {
                    StartCoroutine(GetToMainMenu());
                }
            }
        }

        // Bricht das Tutorial vorzeitig ab
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.Q))
            StartCoroutine(GetToMainMenu());
    }
    
    // Startet eine Sequenz um wieder zum Startbildschirm zu gelangen
    // Lässt den Spieler aus der Sicht aller Gegner verschwinden
    // Blockt die Bewegungen des Spielers
    // Lässt den Bildschirm zu schwarz überblenden
    // Startet am Ende das Hauptmenü
    IEnumerator GetToMainMenu()
    {
        player.layer = 2;
        player.GetComponent<Rigidbody>().isKinematic = true;
        fader.blackLerpSpeed = 2f;
        fader.fadeToBlack = true;
        player.GetComponent<Player_Movement>().enabled = false;
        yield return new WaitUntil(() => fader.blackForegorundColor.a > 0.99f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(0);
    }

    // Stellt den Spieler im Falle eines Todes wieder her
    // Zusätzliche Blendeffekte werden abgespielt um den Übergang flüssiger zu gestalten
    IEnumerator RespawnPlayer()
    {
        fader.blackLerpSpeed = 5f;
        fader.fadeToBlack = true;
        while (fader.blackForegorundColor.a < 0.99f)
        {
            yield return new WaitForFixedUpdate();
        }
        player.transform.position = spawnPosition;
        player.transform.rotation = spawnRotation;
        player.GetComponent<Rigidbody>().Sleep();
        yield return new WaitForSeconds(1f);
        fader.blackLerpSpeed = 0.5f;
        fader.fadeToBlack = false;
        playerHealth.ResetPlayer();
    }
}
