﻿using UnityEngine;
using System.Collections;

//Repräsentiert alle alle für die Animation relevanten Referenzen
public class HashIDs : MonoBehaviour
{
    public int dyingState;
    public int deadBool;
    public int locomotionState;
    public int speedFloat;
    public int npcShootingBool;
    public int shotFloat;
    public int aimWeightFloat;
    public int angularSpeedFloat;
    public int openBool;

    void Awake()
    {
        dyingState = Animator.StringToHash("Base Layer.Dying");
        locomotionState = Animator.StringToHash("Base Layer.Locomotion");
        deadBool = Animator.StringToHash("Dead");
        speedFloat = Animator.StringToHash("Speed");
        npcShootingBool = Animator.StringToHash("Shoot");
        shotFloat = Animator.StringToHash("Shot");
        aimWeightFloat = Animator.StringToHash("AimWeight");
        angularSpeedFloat = Animator.StringToHash("AngularSpeed");
        openBool = Animator.StringToHash("Open");
    }
}
