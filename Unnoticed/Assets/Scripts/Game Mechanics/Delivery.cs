﻿using UnityEngine;
using System.Collections;

// Zuständig für das Pulsieren der Lieferungen
public class Delivery : MonoBehaviour {
    
    private bool glow = false;
    private float glowSpeed = 3f;
    private MeshRenderer[] subRenderer;
    private MainLevelController levelController;

    void Awake()
    {
        subRenderer = GetComponentsInChildren<MeshRenderer>();
        levelController = GameObject.FindGameObjectWithTag(Tags.levelController).GetComponent<MainLevelController>();
    }

	void Update()
    {
        if (levelController)
        {
            if (levelController.deliveriesMarked > 1)
                glow = false;
        }
        #region Leucht Transparenz
        if (glow)
        {
            foreach(MeshRenderer m in subRenderer)
            {
                m.material.SetFloat("_Transparency", (Mathf.Sin(Time.time * glowSpeed) + 1) / 2);
            }
        }
        else
        {
            foreach (MeshRenderer m in subRenderer)
            {
                m.material.SetFloat("_Transparency", 0f);
            }
        }
        #endregion
    }

    public bool Glow
    {
        get
        {
            return glow;
        }
        set
        {
            glow = value;
        }
    }
}
