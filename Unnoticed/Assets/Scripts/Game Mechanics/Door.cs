﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent (typeof(AudioSource))]
public class Door : MonoBehaviour {

    public float smoothSpeed = 1.5f;
    public AudioClip doorSoundOpen;
    public AudioClip doorSoundClose;
    public GameObject doorDoor;

    [HideInInspector]
    public bool open = false;
    private bool turning = false;

    private Quaternion initialRot;
    private Quaternion toRightRot;
    private Quaternion toLeftRot;
    
    void Awake()
    {
        initialRot = doorDoor.transform.localRotation;
        toRightRot = Quaternion.Euler(new Vector3(initialRot.x, initialRot.y + 90f, initialRot.z));
        toLeftRot = Quaternion.Euler(new Vector3(initialRot.x, initialRot.y - 90f, initialRot.z));
    }

    void Update()
    {
        Quaternion currentRotation = doorDoor.transform.localRotation;
        if(currentRotation == initialRot || currentRotation == toLeftRot || currentRotation == toRightRot)
        {
            turning = false;           
        }

        if(currentRotation != initialRot)
            open = true;        
        else
            open = false;
    }

    // Startet je nach Rotation der Tür versch. Bewegungen
    public void TurnDoor(Vector3 position)
    {
        if (!turning)
        {
            Quaternion currentRotation = doorDoor.transform.localRotation;
            if (currentRotation == toRightRot || currentRotation == toLeftRot)
            {
                turning = true;
                StartCoroutine(SmoothMove(initialRot, true));
                return;
            }

            float angle = Toolbox.Spatial.CalcAngle(transform.position, position, -transform.forward, true);
            turning = true;
            if (angle > 180f)
            {
                StartCoroutine(SmoothMove(toLeftRot, false));
            }
            else
            {
                StartCoroutine(SmoothMove(toRightRot, false));
            }
        }
    }
    
    // Bewegt die Tür zur gewünschten Rotation
    // Spielt außerdem Sounds beim schließen/öffnen ab
    public IEnumerator SmoothMove(Quaternion rotation, bool close)
    {
        if (!close)
        {
            AudioSource.PlayClipAtPoint(doorSoundOpen, transform.position, 1f);
            PlayerTracker.DoorMoved(transform.position, 0f);
        }

        float t = 0.0f;
        bool closeSoundPlayed = false;
        while (t <= 1.0f)
        {
            t += Time.deltaTime / smoothSpeed;
            doorDoor.transform.localRotation = Quaternion.Lerp(doorDoor.transform.localRotation, rotation, Mathf.SmoothStep(0.0f, 1.0f, t));
            if(t > 0.3f && close && !closeSoundPlayed)
            {
                AudioSource.PlayClipAtPoint(doorSoundClose, transform.position, 1f);
                PlayerTracker.DoorMoved(transform.position, 0f);
                closeSoundPlayed = true;
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
