﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

// Zuständig für das Verhalten des In-game UIs
public class GameUI : MonoBehaviour {

    public GameObject PauseMenu;
    [HideInInspector]
    public bool menuOn = false;

    public Text contextMessage;
    private GameObject player;
    private Camera playerViewport;
    private Blur viewportBlur;
    private Player_Input playerInput;
    private MainLevelController levelController;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        if(player != null)
        {
            playerViewport = player.GetComponentInChildren<Camera>();
            viewportBlur = player.GetComponentInChildren<Blur>();
            playerInput = player.GetComponent<Player_Input>();
        }
        contextMessage.text = "";
        
        if (SceneManager.GetActiveScene().buildIndex == 2)
            levelController = GameObject.FindGameObjectWithTag(Tags.levelController).GetComponent<MainLevelController>();
    }

    void Update()
    {
        // Debug Option, Schaltet den Cursor frei während das Spiel läuft
        if (Input.GetKeyDown(KeyCode.P))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if(player != null)
        {
            if (Input.GetKeyDown(playerInput.pauseKey))
            {
                menuOn = !menuOn;
            }
        }

        UpdatePauseMenuState();
        CheckForContext();
    }

    // Aktualisiert den Status des Pause Menüs
    void UpdatePauseMenuState()
    {
        if (menuOn)
        {
            Time.timeScale = 0f;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            PauseMenu.SetActive(true);
            contextMessage.text = "";
            viewportBlur.enabled = true;
        }
        else
        {
            Time.timeScale = 1f;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            PauseMenu.SetActive(false);
            viewportBlur.enabled = false;
        }
    }

    // Überprüft, ob der Spieler auf etwas in Reichweite zielt was man "nutzen" kann
    // Je nach Objekt werden versch. Kontextnachrichten angezeigt
    void CheckForContext()
    {
        RaycastHit hit;
        if(Physics.Raycast(playerViewport.ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2)), out hit, playerInput.useDistance))
        {
            GameObject hitThing = hit.collider.gameObject;
            string tag = hit.collider.gameObject.tag;
            switch (tag)
            {
                case Tags.door:
                    Door doorComp = hitThing.GetComponentInParent<Door>();
                    if (doorComp)
                    {
                        if(doorComp.open)
                            contextMessage.text = "[" + playerInput.useKeyD + "] oder [0] zum schließen";
                        else
                            contextMessage.text = "[" + playerInput.useKeyD + "] oder [0] zum öffnen";
                    }
                    break;
                case Tags.item:
                    contextMessage.text = "[" + playerInput.useKeyD + "] oder [0] zum einsammeln";
                    break;
                case Tags.gate:
                    if (levelController.deliveriesMarked > 1)
                        contextMessage.text = "[" + playerInput.useKeyD + "] oder [0] zum abschließen";
                    break;
                case Tags.delivery:
                    if (levelController.itemCollected)
                    {
                        if (hitThing.GetComponentInParent<Delivery>().Glow)
                            contextMessage.text = "[" + playerInput.useKeyD + "] oder [0] zum markieren";
                        else
                            contextMessage.text = "";
                    }
                    break;
                default:
                    contextMessage.text = "";
                    break;
            }
        }
        else
        {
            contextMessage.text = "";
        }
    }

    // Schaltet das Pause Menü um
    public void TogglePauseMenu()
    {
        menuOn = !menuOn;
    }

    // Statet das Level neu
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Beendet den Prototypen
    public void QuitGame()
    {
        Application.Quit();   
    }

    // Kehrt zum Hauptmenü zurück
    public void ReturnToMain()
    {
        SceneManager.LoadScene(0);
    }
}
