﻿using UnityEngine;
using System.Collections;

// Zuständig für das Pulsieren des Items im Spiel
public class Item : MonoBehaviour {

    private bool glow = true;
    private float glowSpeed = 3f;
    private MeshRenderer[] subRenderer;

    void Awake()
    {
        subRenderer = GetComponentsInChildren<MeshRenderer>();
    }

    void Update()
    {
        #region Leucht Transparenz
        if (glow)
        {
            foreach (MeshRenderer m in subRenderer)
            {
                m.material.SetFloat("_Transparency", (Mathf.Sin(Time.time * glowSpeed) + 1) / 2);
            }
        }
        else
        {
            foreach (MeshRenderer m in subRenderer)
            {
                m.material.SetFloat("_Transparency", 0f);
            }
        }
        #endregion
    }

    public bool Glow
    {
        get
        {
            return glow;
        }
        set
        {
            glow = value;
        }
    }
}
