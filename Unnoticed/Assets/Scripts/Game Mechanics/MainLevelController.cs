﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using System.Collections;

// Zuständig für das Hauptlevel
public class MainLevelController : MonoBehaviour {

    public Text introText;

    public string[] tasks = {
        "Begebe dich in das Büro am anderen Ende der Halle -",
        "Finde die Zielliste auf einem der Schreibtische -",
        "Finde und markiere 2 Lieferungen im Lager -",
        "Entkomme durch das Haupttor am Start -",
        "Vorsicht vor den Wachen! -"
    };
    
    [HideInInspector]
    public int deliveriesMarked = 0;
    [HideInInspector]
    public bool itemCollected = false;

    private int deaths = 0;
    private bool died = false;
    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private GameObject player;
    private Player_Health playerHealth;
    private Camera_UI fader;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerHealth = player.GetComponent<Player_Health>();
        fader = player.GetComponentInChildren<Camera_UI>();
        spawnPosition = player.transform.position;
        spawnRotation = player.transform.rotation;
        if (introText)
            introText.text = "";
    }

    void Start()
    {
        StartCoroutine(BeginGame());
    }

    void Update()
    {
        // Startet den Spieler neu, falls dieser stirbt
        if (playerHealth.health <= 0f && died == false)
        {
            died = true;
            deaths++;
            StartCoroutine(RespawnPlayer());
        }

        if(Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.Q))
        {
            StartCoroutine(GetToMainMenu());
        }
    }

    // Schaltet die Lieferungen zum Einsammeln frei sobald die Liste eingesammelt wurde
    public void ItemCollected()
    {
        itemCollected = true;
        GameObject[] deliveries = GameObject.FindGameObjectsWithTag(Tags.delivery);
        foreach (GameObject g in deliveries)
        {
            try
            {
                g.GetComponent<Delivery>().Glow = true;
            }
            catch(Exception e)
            {
                Debug.Log(e);
            }
        }
    }
    
    public void WinGame()
    {
        StartCoroutine(GetToMainMenu());
    }

    // Startet eine Sequenz am Anfang des Levels
    // Schaltet den Spieler frei nachdem der Missionstext angezeigt wurde
    IEnumerator BeginGame()
    {
        fader.fadeToBlack = true;
        yield return new WaitForSeconds(1f);
        for(int i = 0; i < tasks.Length; i++)
        {
            introText.text += tasks[i] + "\n";
            yield return new WaitForSeconds(4f);
        }
        yield return new WaitForSeconds(3f);
        fader.fadeToBlack = false;
        while(introText.color != Color.clear)
        {
            introText.color = Color.Lerp(introText.color, Color.clear, 0.5f);
            yield return new WaitForFixedUpdate();
        }
        player.GetComponent<Rigidbody>().isKinematic = false;
        GameObject[] npcs = GameObject.FindGameObjectsWithTag(Tags.npc);
        foreach(GameObject g in npcs)
        {
            g.GetComponent<NPC_AI>().enabled = true;
        }
    }
    
    // Startet eine Sequenz zum Ende des Spiels
    IEnumerator GetToMainMenu()
    {
        #region Deaktiviert den Spieler
        player.layer = 2;
        fader.blackLerpSpeed = 2f;
        fader.fadeToBlack = true;
        player.GetComponent<Player_Movement>().enabled = false;
        player.GetComponent<Rigidbody>().isKinematic = true;
        yield return new WaitUntil(() => fader.blackForegorundColor.a > 0.99f);
        #endregion

        #region zeigt Missionsdaten
        introText.text = "";
        introText.color = Color.white;
        introText.text += "Mission abgeschlossen\n";
        yield return new WaitForSeconds(2f);
        introText.text += "Zeit: " + (int)Time.timeSinceLevelLoad/60 + " Minute(n), " + (int)(Time.time % 60) + " Sekunde(n)" + "\n";
        #endregion

        #region speichert Log
        string filename = "P" + GameStats.testID + "_" + System.DateTime.Now.ToString("ddMMyyyymmHH") + ".txt";
        if (!File.Exists(filename))
        {
            StreamWriter stream = File.CreateText(filename);
            stream.WriteLine("Zeit: " + Time.time);
            stream.WriteLine("Formatiert: " + (int)Time.timeSinceLevelLoad / 60 + " Minute(n), " + (int)Time.time % 60 + " Sekunden");
            stream.WriteLine("Tode: " + deaths);
            stream.WriteLine("SVI: " + (GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<GameStats>().SVIDirect ? "Direkt" : "Indirekt"));
            stream.Close();
        }
        yield return new WaitForSeconds(2f);
        #endregion

        // Zeigt Todeszahl
        introText.text += "Tode: " + deaths + "\n";
        yield return new WaitForSeconds(6f);
        SceneManager.LoadScene(0);
    }

    // Startet den Spieler neu
    // Zusätzliche Effekte werden abgespielt um den Übergang fließender ablaufen zu lassen
    IEnumerator RespawnPlayer()
    {
        fader.blackLerpSpeed = 2f;
        fader.fadeToBlack = true;
        while (fader.blackForegorundColor.a < 0.99f)
        {
            yield return new WaitForFixedUpdate();
        }
        player.transform.position = spawnPosition;
        player.transform.rotation = spawnRotation;
        player.GetComponent<Rigidbody>().Sleep();
        yield return new WaitForSeconds(1f);
        fader.blackLerpSpeed = 0.5f;
        fader.fadeToBlack = false;
        playerHealth.ResetPlayer();
        died = false;
    }
}
