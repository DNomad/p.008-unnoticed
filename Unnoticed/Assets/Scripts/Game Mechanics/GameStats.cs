﻿using UnityEngine;
using System.Collections;

// Zuständig für diverse Levelübergrreifende Variablen
public class GameStats : MonoBehaviour {

    public bool SVIDirect = false;
    public bool SVIIndirect = false;

    public static GameStats gameStats;

    public static string testID = "";

    void Awake()
    {
        DontDestroyOnLoad(this);

        if(gameStats == null)
        {
            DontDestroyOnLoad(gameObject);
            gameStats = this;
        }else if(gameStats != null)
        {
            Destroy(gameObject);
        }
    }
    
    // Reaktiviert den Cursor, falls das Hauptmenü geladen wurde
    void OnLevelWasLoaded(int index)
    {
        if (index == 0)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
