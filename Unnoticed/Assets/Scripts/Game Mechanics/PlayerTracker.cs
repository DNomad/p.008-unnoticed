﻿using UnityEngine;
using System.Collections;

// Verfolgt den aktuellen Status des Spielers
public class PlayerTracker : MonoBehaviour
{
    public delegate void SoundEvent (Vector3 position, float intensity);
    public static SoundEvent ShotFired;
    public static SoundEvent Stepped;
    public static SoundEvent Jumped;
    public static SoundEvent DoorMoved;

    public Vector3 globalLastKnownPosition = new Vector3(1000f, 1000f, 1000f);
    public Vector3 resetPosition = new Vector3(1000f, 1000f, 1000f);
    public Vector3 currentPlayerPosition;
    public static float playerVisibilityBase = 10f;
    
    private GameObject player;

    void Start()
    {
        Init();
    }

    // Zuweisung der Variablen
    void Init()
    {
        currentPlayerPosition = resetPosition;
        player = GameObject.FindGameObjectWithTag(Tags.player);
    }

    void Update()
    {
        if(player != null)
        {
            currentPlayerPosition = player.transform.position;
        }
    }

    void OnLevelWasLoaded(int index)
    {
        if (index > 0)
            Init();
    }
}
    

