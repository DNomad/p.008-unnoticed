﻿using UnityEngine;
using System.Collections;

// Liste von allen AudioAsset Referenzen
public class AudioAssets : MonoBehaviour {

    public AudioClip[] steps;
    public AudioClip[] jumps;
    public AudioClip[] landing;
    public AudioClip[] breath;
    public AudioClip[] heartbeat;
    public AudioClip[] voicesIntrigued;
    public AudioClip[] voicesCalmed;
    public AudioClip[] shots;
    public AudioClip[] doorOpen;
    public AudioClip[] doorClose;
    public AudioClip[] ambience;
    public AudioClip[] misc;
    public AudioClip[] lighter;
    public AudioClip[] zippo;
}
