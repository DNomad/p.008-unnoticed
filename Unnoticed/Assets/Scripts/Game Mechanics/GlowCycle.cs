﻿using UnityEngine;
using System.Collections;
using System;

// Lässt relevante Objekte weiß pulsieren
public class GlowCycle : MonoBehaviour {

    private MeshRenderer[] subRenderer;
    private float glowSpeed = 3f;

    void Awake()
    {
        subRenderer = GetComponentsInChildren<MeshRenderer>();
    }

    void Update()
    {
        foreach(MeshRenderer m in subRenderer){
            try
            {
                m.material.SetFloat("_Transparency", (Mathf.Sin(Time.time * glowSpeed) + 1) / 2);
            }catch(Exception e)
            {
                Debug.Log(e);
            }
        }
    }
}
