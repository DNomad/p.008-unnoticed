﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

// Zuständig für das Verhalten des Hauptmenüs
public class MainMenu : MonoBehaviour {
    
    public Toggle sviIToggle;
    public Toggle sviDToggle;
    public InputField IDInput;

    private GameStats gameStats;

    void Awake()
    {
        gameStats = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<GameStats>();
        sviDToggle.isOn = gameStats.SVIDirect;
        sviIToggle.isOn = gameStats.SVIIndirect;
    }
    
    void Update()
    {
        gameStats.SVIIndirect = sviIToggle.isOn;
        gameStats.SVIDirect = sviDToggle.isOn;

        // Überprüft, was innerhalb des Input Felds steht und setzt die Hintergrundfarbe zurück
        ColorBlock cb = IDInput.colors;
        cb.normalColor = Color.Lerp(cb.normalColor, Toolbox.Mathfx.HexToColor("FFFFFF01"), 0.5f);
        IDInput.colors = cb;
        Regex rgx = new Regex("[^0-9]");
        IDInput.text = rgx.Replace(IDInput.text, "");
        if (IDInput.text.Length > 2)
            IDInput.text = IDInput.text.Substring(0, 2);
    }
    
    // Startet das Hauptlevel
    public void LoadMainLevel()
    {
        // Lässt das Inputfeld für die Probandennummer aufleuchten, falls keine passende Eingabe erfolgt ist
        if (IDInput.text.Length == 2)
        {
            GameStats.testID = IDInput.text;
            SceneManager.LoadScene(2);
        }
        else
        {
            ColorBlock cb = IDInput.colors;
            cb.normalColor = Color.red;
            IDInput.colors = cb;
        }
    }

    // Startet das Tutorial Level
    public void LoadTutorial()
    {
        SceneManager.LoadScene(1);
    }
    
    // Beendet den Prototypen
    public void QuitGame()
    {
        Application.Quit();
    }
}
