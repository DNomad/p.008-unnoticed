﻿using UnityEngine;
using System.Collections;

namespace Toolbox
{
    public class Spatial : MonoBehaviour
    {
        // Berechnet einen 0°-360° Winkel zwischen 2 Vektoren
        public static float CalcAngle(Vector3 v1, Vector3 v2, Vector3 forward, bool sightAngle)
        {
            float result = 0f;

            Vector3 dir = Vector3.ProjectOnPlane(Vector3.right, Vector3.down);
            if(sightAngle)
            {
                dir = Vector3.ProjectOnPlane(forward, Vector3.down);
            }
            Vector3 pos = Vector3.ProjectOnPlane(v1, Vector3.down);
            Vector3 to = Vector3.ProjectOnPlane(v2, Vector3.down);
            Vector3 diff = to - pos;

            result = Vector3.Angle(dir, diff);
            Vector3 cross = Vector3.Cross(dir, diff);
            if (cross.y > 0)
            {
                result = 360 - result;
            }
            return result;
        }
        
        // Berechnet die Distanz zwischen 2 Vektoren unabhängig von deren Höhe
        public static float CalcDistance(Vector3 from, Vector3 to)
        {
            float result = 0f;
            result = Vector3.Distance(Vector3.ProjectOnPlane(from, Vector3.down), Vector3.ProjectOnPlane(to, Vector3.down));
            return result;
        }

        // Berechnet die Distanz zwischen 2 Vektoren auf einem NavMesh
        public static float CalcPathDistance(Vector3 originPos, Vector3 targetPos, ref UnityEngine.AI.NavMeshAgent nav)
        {
            UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
            if (nav.enabled)
                nav.CalculatePath(targetPos, path);
            Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];
            allWayPoints[0] = originPos;
            allWayPoints[allWayPoints.Length - 1] = targetPos;
            for (int i = 0; i < path.corners.Length; i++)
            {
                allWayPoints[i + 1] = path.corners[i];
            }

            float pathLength = 0;
            for (int i = 0; i < allWayPoints.Length - 1; i++)
            {
                pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
            }

            return pathLength;
        }
    }
}
