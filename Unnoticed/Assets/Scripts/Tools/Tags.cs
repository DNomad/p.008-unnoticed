﻿using UnityEngine;
using System.Collections;

// Liste von allen Tags als statische Referenzen
public class Tags : MonoBehaviour
{
    public const string player = "Player";
    public const string playerHead = "PlayerHead";
    public const string playerViewport = "PlayerViewport";
    public const string gameController = "GameController";
    public const string fader = "Fader";
    public const string npc = "NPC";
    public const string door = "Door";
    public const string item = "Item";
    public const string contextMessage = "ContextMessage";
    public const string gate = "Gate";
    public const string delivery = "Delivery";
    public const string levelController = "LevelController";
}