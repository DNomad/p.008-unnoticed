﻿using UnityEngine;
using System.Collections;

// Zuständig dafür, ein Kabel für eine Hängelampe mithilfe eines LineRenderers zu erstelen
public class Leuchte01Kabel : MonoBehaviour {
    
    public Transform startPoint;
    public Transform endPoint;

    void Awake()
    {
        if(startPoint && endPoint && GetComponent<LineRenderer>())
        {
            Vector3[] points = { startPoint.position, endPoint.position };
            GetComponent<LineRenderer>().SetPositions(points);
        }
    }
}
