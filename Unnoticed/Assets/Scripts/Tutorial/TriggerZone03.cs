﻿using UnityEngine;
using System.Collections;

public class TriggerZone03 : MonoBehaviour {

    public bool stopsPlayer = true;

    private GameObject player;
    private Player_Input playerInput;
    private TutorialController tutorialController;

	void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerInput = player.GetComponent<Player_Input>();
        tutorialController = GameObject.Find("TutorialController").GetComponent<TutorialController>();
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(playerInput.crouchKeyD) || Input.GetKeyDown(playerInput.crouchKeyL))
        {
            player.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player && stopsPlayer)
        {
            player.GetComponent<Rigidbody>().isKinematic = true;
        }
        tutorialController.spawnPosition = new Vector3(transform.position.x, 1f, transform.position.z);
        tutorialController.spawnRotation = Quaternion.Euler(new Vector3(0, 270, 0));
    }
}
