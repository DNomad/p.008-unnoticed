﻿using UnityEngine;
using System.Collections;

public class TriggerZone06 : MonoBehaviour {

    private GameObject player;
    private TutorialController tutorialController;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        tutorialController = GameObject.Find("TutorialController").GetComponent<TutorialController>();
    }

	void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            tutorialController.spawnPosition = new Vector3(transform.position.x, 1f, transform.position.z);
            tutorialController.spawnRotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
    }
}
