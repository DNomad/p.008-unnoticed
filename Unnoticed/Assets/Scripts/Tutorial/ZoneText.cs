﻿using UnityEngine;
using UnityEngine.UI;

// Zeigt einen Text für die entsprechende Zone
public class ZoneText : MonoBehaviour {

    [TextArea]
    public string message = "";
    public bool resetTextOnExit = false;
    public Text subtitle;

    private GameObject player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
    }
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            subtitle.text = message;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player && resetTextOnExit)
        {
            subtitle.text = "";
        }
    }
}
