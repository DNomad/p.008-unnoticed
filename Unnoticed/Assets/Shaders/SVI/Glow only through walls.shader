﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "SVI/Glow only through walls" {
	Properties{
		_RimCol("Rim Colour" , Color) = (1,1,1,1)
		_RimPow("Rim Power", Range(0, 6.0)) = 2.0
		_Transparency("Transparency", Range(0, 1.0)) = 1.0
	}

	SubShader{
		
		Pass{
			Name "Behind"
			Tags{ "RenderType" = "transparent" "Queue" = "Transparent" }
			// you can choose what kind of blending mode you want for the outline
			//Blend SrcAlpha OneMinusSrcAlpha // Normal
			Blend One One // Additive
			//Blend One OneMinusDstColor // Soft Additive
			//Blend DstColor Zero // Multiplicative
			//Blend DstColor SrcColor // 2x Multiplicative
			ZTest Always               // here the check is for the pixel being greater or closer to the camera, in which
			Cull Back                   // case the model is behind something, so this pass runs
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : TEXCOORD1;      // Normal needed for rim lighting
				float3 viewDir : TEXCOORD2;     // as is view direction.
			};

			float4 _RimCol;
			float _RimPow;
			float _Transparency;

			v2f vert(appdata_tan v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = normalize(v.normal);
				o.viewDir = normalize(ObjSpaceViewDir(v.vertex));       //this could also be WorldSpaceViewDir, which would
				return o;                                               //return the World space view direction.
			}

			half4 frag(v2f i) : COLOR
			{
				half Rim = 1 - saturate(dot(normalize(i.viewDir), i.normal));       //Calculates where the model view falloff is for rim lighting.
				float4 col = _RimCol;
				half4 RimOut = _RimCol * pow(Rim, _RimPow);
				return RimOut * _Transparency;
			}
			ENDCG
		}
		
		
		Pass{
			Name "Regular"
			Tags{ "RenderType" = "Opaque" }
			// you can choose what kind of blending mode you want for the outline
			Blend SrcAlpha OneMinusSrcAlpha // Normal
			//Blend One One // Additive
			//Blend One OneMinusDstColor // Soft Additive
			//Blend DstColor Zero // Multiplicative
			//Blend DstColor SrcColor // 2x Multiplicative
			ZTest LEqual                // this checks for depth of the pixel being less than or equal to the shader
			ZWrite On                   // and if the depth is ok, it renders the main texture.
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			float4 _RimCol;
			float _Transparency;

			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				return _RimCol * _Transparency;
			}
			ENDCG
		}
	}
	FallBack "VertexLit"
}