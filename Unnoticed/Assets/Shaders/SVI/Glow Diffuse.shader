﻿// Standard Material Modell vom Glow Shader (ohne Durchsicht)
Shader "SVI/Glow Diffuse"{
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.0
		_Metallic("Metallic", Range(0,1)) = 0.0
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimPower("Rim Power", Range(0, 6.0)) = 2.0
		_Transparency("Transparency", Range(0, 1.0)) = 1.0
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		float4 _RimColor;
		float _RimPower;
		float _Transparency;

		void surf(Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;

			half rim = (1.0 - saturate(dot(normalize(IN.viewDir), o.Normal))) * _Transparency;
			float4 col = _RimColor;
			col.a *= _Transparency;
			o.Emission = col * pow(rim, _RimPower);
		}
		ENDCG
	}
	FallBack "Diffuse"
}

/*Shader "SVI/Glow" {
Properties {
_ColorTint("Color Tint", Color) = (1,1,1,1)
_MainTex("Base (RGB)", 2D) = "white" {}
_RimColor("Rim Color", Color) = (1,1,1,1)
_RimPower("Rim Power", Range(0, 6.0)) = 0.5
_Transparency ("Transparency", Range(0, 1.0)) = 1.0
}

SubShader {
Tags{ "RenderType" = "Opaque" }

CGPROGRAM
#pragma surface surf Standard fullforwardshadows

struct Input {
float4 color : Color;
float2 uv_MainTex;
float2 uv_BumpMap;
float3 viewDir;
};

float4 _ColorTint;
sampler2D _MainTex;
sampler2D _BumpMap;
float4 _RimColor;
float _RimPower;
float _Transparency;

void surf(Input IN, inout SurfaceOutputStandard o) {
IN.color = _ColorTint;
o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * IN.color;
o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

half rim = (1.0 - saturate(dot(normalize(IN.viewDir), o.Normal))) * _Transparency;
float4 col = _RimColor;
col.a *= _Transparency;
o.Emission = col * pow(rim, _RimPower);
}

ENDCG
}
FallBack "Diffuse"
}*/