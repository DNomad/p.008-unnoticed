﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "SVI/SVI_I"
{
	Properties
	{
		_RingColor ("Ring Color", Color) = (1,1,1,1)
		_Transparency ("Transparency", Range(0, 1.0)) = 1.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="transparent"}

		Pass
		{
			ZWrite On
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float4 _RingColor;
			float _Transparency;

			fixed4 frag (v2f i) : COLOR
			{
				fixed4 col = _RingColor * _Transparency;
				col.a = _Transparency;
				return col;
			}
			ENDCG
		}
	}
}
