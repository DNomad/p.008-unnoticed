﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "SVI/Outline Only Pass" {
	Properties{
		_OutlineColor("Outline Color", Color) = (1,1,1,1)
		_Outline("Outline width", Range(0.0, 1.0)) = 0.005
	}

	SubShader{
		Pass{
			Name "Behing"
			Tags {"RenderType" = "transparent" "Queue" = "Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Always
			Cull Front
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert	
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : POSITION;
				float4 color : COLOR;
				float3 normal : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
			};

			uniform float _Outline;
			uniform float4 _OutlineColor;

			v2f vert(appdata_tan v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = normalize(v.normal);
				o.viewDir = normalize(ObjSpaceViewDir(v.vertex));
				return o;
			}

			half4 frag(v2f i) : COLOR{
				return i.color;
			}

			ENDCG
		}
	}
	Fallback "Diffuse"
}